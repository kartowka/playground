# The Grandest Staircase Of Them All
# ==================================

# With the LAMBCHOP doomsday device finished, Commander Lambda is preparing to debut on the galactic stage -- but in order to make a grand entrance, Lambda needs a grand staircase! As the Commander's personal assistant, you've been tasked with figuring out how to build the best staircase EVER. 

# Lambda has given you an overview of the types of bricks available, plus a budget. You can buy different amounts of the different types of bricks (for example, 3 little pink bricks, or 5 blue lace bricks). Commander Lambda wants to know how many different types of staircases can be built with each amount of bricks, so they can pick the one with the most options. 

# Each type of staircase should consist of 2 or more steps.  No two steps are allowed to be at the same height - each step must be lower than the previous one. All steps must contain at least one brick. A step's height is classified as the total amount of bricks that make up that step.
# For example, when N = 3, you have only 1 choice of how to build the staircase, with the first step having a height of 2 and the second step having a height of 1: (# indicates a brick)

# #
# ##
# 21

# When N = 4, you still only have 1 staircase choice:

# #
# #
# ##
# 31
 
# But when N = 5, there are two ways you can build a staircase from the given bricks. The two staircases can have heights (4, 1) or (3, 2), as shown below:

# #
# #
# #
# ##
# 41

# #
# ##
# ##
# 32

# Write a function called solution(n) that takes a positive integer n and returns the number of different staircases that can be built from exactly n bricks. n will always be at least 3 (so you can have a staircase at all), but no more than 200, because Commander Lambda's not made of money!

# Languages
# =========

# To provide a Java solution, edit Solution.java
# To provide a Python solution, edit solution.py

# Test cases
# ==========
# Your code should pass the following test cases.
# Note that it may also be run against hidden test cases not shown here.

# -- Python cases --
# Input:
# solution.solution(200)
# Output:
#     487067745

# Input:
# solution.solution(3)
# Output:
#     1

# Use verify [file] to test your solution and see how it does. When you are finished editing your code, use submit [file] to submit your answer. If your solution passes the test cases, it will be removed from your home folder.

from typing import Counter


class Solution:

    def __init__(self):
        self.cache = {
            0: [[0]],
            1: [[1]],
            2: [[2]],
            3: [[3],[2,1]],
        }

    def solve(self, n):

        if n in self.cache:
            # print("cached", n)
            return self.cache[n]

        res = [[n]]
        was_in_break = False
        for i in range(n-1, 1, -1):
            for r in self.solve(n - i)[::-1]:
                if i > r[0]:
                    # print([i] + r)
                    res.append([i] + r)
                    if was_in_break:
                        pass
                else:
                    was_in_break = True
                    break

        self.cache[n] = res
        return res


class Solution1:

    def __init__(self):
        self.cache = {
            0: [[0]],
            1: [[1]],
            2: [[2]],
            3: [[3],[2,1]],
        }

    def solve(self, max_element, n):

        if n in self.cache:
            # print("cached", max_element)
            return self.cache[n]

        res = []
        max_element = min(max_element, n)
        for i in range(max_element-1, 2, -1):
            for r in self.solve(i, n - i)[::-1]:
                if i > r[0]:
                    # print([i] + r)
                    res.append([i] + r)
                else:
                    break

        self.cache[n] = res
        return res

class Solution2:

    def __init__(self):
        self.cache = {
            0: [0],
            1: [1],
            2: [2],
            3: [3,2],
        }

    def solve(self, max_element, n):

        if n in self.cache:
            # print("cached", max_element)
            return self.cache[n]
        
        res = [n]
        max_element = min(max_element, n)
        s = 1
        for i in range(max_element-1, 2, -1):
            for r in self.solve(i, n - i)[::-1]:
                if i > r:
                    # print([i])
                    s += 1
                    res.append(i)
                else:
                    break
        
        # print(n, "->", s-1, s/(time.time() - st))
        self.cache[n] = res
        return res


s = Solution()
s1 = Solution1()
s2 = Solution2()
# f = open("results", "w+")
# res = {}
# for i in range(1, 100):
#     res = s.solve(i)
#     print(i, len(res)-1)

# N = 180 # 487067745
import time
# st = time.time()
# print(N, len(s.solve(N))-1)
# print(time.time() - st)
# print(s.solve(N))
# st = time.time()
# print(len(s1.solve(N, N)))
# print(time.time() - st)

for i in range(1,201):
    st = time.time()
    print(i, len(s2.solve(i, i))-1, time.time() - st)

# import json
# f.write(json.dumps(res))
# f.close()
# import time
# st = time.time()
# print(s.solve(200))
# print(time.time() - st)
# print(i, s.solve(20))
# 8 2
# 7 3  
# 7 2 1
# 6 4  
# 6 3 1
# 5 4 1
# 5 3 2