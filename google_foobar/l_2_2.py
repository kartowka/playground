

class Node(object):
    def __init__(self, val=-1):
        self.val = val
        self.left = None
        self.right = None
    
    
class Solution:
    def __init__(self):
        self.parents = {}
        
    def create_postorder_tree(self, labels, h, parent=-1):
        
        label = labels.pop()
        self.parents[label] = parent
        
        if h == 1:
            return Node(label)
        
        node = Node(label)
        node.right = self.create_postorder_tree(labels, h-1, node.val)
        node.left = self.create_postorder_tree(labels, h-1, node.val)
        return node
    
    def solve(self, h, labels):
        self.create_postorder_tree(list(range(1, 2**h)), h)
        res = []
        for l in labels:
            res.append(self.parents[l])
        return res


def solution(h, labels):
    s = Solution()
    return s.solve(h, labels)
    
print(solution(3, [1,4,7]))
print(solution(3, [7, 3, 5, 1]))
print(solution(5, [19, 14, 28]))
print(solution(1,[1]))