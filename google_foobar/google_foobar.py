
def get_all_radiuses_starting_from_r(pegs, r):
    radiuses = []
    def propagate(pegs, r):
        
        if r < 1:
            return False
        
        radiuses.append(r)
        if len(pegs) == 1:
            return True
        if not propagate(pegs[1:], pegs[1] - (pegs[0] + r)):
            radiuses.pop()
            return False
        return True
    propagate(pegs, r)
    return radiuses

def get_next_R(limit):
    i = 0
    while i <= limit:
        yield i + 1/3
        yield i + 2/3
        i += 1
        yield i


def fast_update(pegs, rs):
    
    if len(pegs) % 2 == 1:
        dx = rs[0] - 2 * rs[-1]
        return [rs[0] + dx, rs[-1] + dx]
    else:
        dx = (2*rs[-1] - rs[0]) / 3
        return [rs[0] + dx, rs[-1] - dx]

def solution(pegs):
    initial_values = []
    limit = pegs[1] - pegs[0]

    for r in get_next_R(limit):

        if not initial_values:
            initial_values = get_all_radiuses_starting_from_r(pegs, r)
            rs = initial_values
        else:
            rs = fast_update(pegs, rs)
            if not rs or rs[0] < 1 or rs[1] < 1 \
                or not get_all_radiuses_starting_from_r(pegs, rs[0]):
                    return [-1, -1]

        if not rs: continue
        if (rs[0] * 10000000000000) // 100000000000 == (2 * rs[-1] * 10000000000000) // 100000000000:
            if int(rs[0]) == rs[0]:
                return [int(rs[0]), 1]
            return [round(rs[0] * 3), 3] 
    return [-1, -1]


# print(solution([0,2,4,6]))
# print(solution([0, 15, 17, 19]))
# print(solution([0, 12, 17, 19]))
# solution([0,55,62,72])
# res = solution([0, 6, 11, 17])
print(solution([4, 30001, 50011, 62011, 75047]))

# res = solution([0, 5000, 10000])
# print(res)


# results = []
# import time
# L = 150
# for i1 in range(100, L, 1):
#     for i2 in range(i1+1, L):
#         for i3 in range(i2+1, L):
#             st = time.time()
#             res = solution([i1, i2, i3])
#             dt = time.time() - st
#             if res != [-1, -1]:
#                 results.append([i1, i2, i3, tuple(res)])

# for r in sorted(results):
#     print(r)

# print(len(results))