
def get_radiuses_starting_from_r(pegs, r):
    """Returns first mathching sequence of radiuses for given pegs.
    """
    radiuses = []
    def propagate(pegs, r):        
        if r < 1:
            return False
        radiuses.append(r)
        if len(pegs) == 1:
            return True
        if not propagate(pegs[1:], pegs[1] - (pegs[0] + r)):
            radiuses.pop()
            return False
        return True
    propagate(pegs, r)
    return radiuses

def fast_update(pegs, radiuses, dr):
    """Given pegs configuration and any matching sequence of radiuses, updates the first
    and last gears according to given dr
    """
    if radiuses[0] + dr < 1 or pegs[0] + radiuses[0] + dr > pegs[1] - 1:
        return []
    
    if len(pegs) % 2 == 1:
        if radiuses[-1] + dr < 1 or pegs[-1] - radiuses[-1] - dr < pegs[-2] + 1:
            return []
        return radiuses[0] + dr, radiuses[-1] + dr
    else:
        if radiuses[-1] - dr < 1 or pegs[-1] - radiuses[-1] + dr < pegs[-2] + 1:
            return []
        return radiuses[0] + dr, radiuses[-1] - dr 


def binary_fraction_search(pegs):
    """Binary search for a first radius that matches the sequence
    """
    lfrac = [0, 1]
    rfrac = [1, 0]
    c0 = 0
    R = 1
    radiuses = []
    
    if len(pegs) == 2:
        p = (pegs[1] - pegs[0]) / 3
        if int(p) == p:
            return [int(2*p), 1]
        return [round(6*p), 3]

    while True:
        m_numerator = lfrac[0] + rfrac[0]
        m_denumerator = lfrac[1] + rfrac[1]
        old_R = R
        R = float(m_numerator) / m_denumerator
        if pegs[0] + R > pegs[1] - 1:
            return [-1, -1]

        if not radiuses:
            if c0 != 0:
                return [-1, -1]
            radiuses = get_radiuses_starting_from_r(pegs, R)
            if not radiuses:
                lfrac = [m_numerator, m_denumerator]
                continue
        else:
            radiuses = fast_update(pegs, radiuses, R-old_R)
            if not radiuses or radiuses[0] < 1 or radiuses[1] < 1 \
                or not get_radiuses_starting_from_r(pegs, radiuses[0]):
                    return [-1, -1]
        
        c0 = int(radiuses[0] * 10000000000000)
        c1 = int(radiuses[-1] * 200000000000000) // 10
        if len(pegs) % 2 == 1:
            if c0 < c1:
                rfrac = [m_numerator, m_denumerator]
            elif c0 > c1:
                lfrac = [m_numerator, m_denumerator]
            else:
                return m_numerator, m_denumerator
        else:
            if c0 < c1:
                lfrac = [m_numerator, m_denumerator]
            elif c0 > c1:
                rfrac = [m_numerator, m_denumerator]
            else:
                return m_numerator, m_denumerator

def solution(pegs):
    return binary_fraction_search(pegs)


results = []
import time
L = 50
for i1 in range(L):
    for i2 in range(i1+1, L):
        for i3 in range(i2+1, L):
            st = time.time()
            res = solution([i1, i2, i3])
            dt = time.time() - st
            if res != [-1, -1]:
                results.append([i1, i2, i3, res])

for r in sorted(results):
    print(r)

print(len(results))